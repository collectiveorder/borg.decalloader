﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Decal.Adapter;

namespace DecalLoader
{
    static class Program
    {
        static XDocument CSProjXmlDoc { get; set; }

        static void Main(string[] args)
        {
#if DEBUG
            //Args to help debug issues with this utility
            //args = new string[] { "pre", @"C:\dev\AsheronsCall\_BorgCollective\Borg.SubSpace\Borg.SubSpace\bin\Debug\Borg.SubSpace.dll", @"C:\dev\AsheronsCall\_BorgCollective\Borg.SubSpace\Borg.SubSpace\Borg.SubSpace.csproj" };
            // args = new string[] { "post", @"C:\dev\AsheronsCall\_BorgCollective\Borg.SubSpace\Borg.SubSpace\bin\Debug\603205091\Borg.SubSpace_603205091.dll", @"C:\dev\AsheronsCall\_BorgCollective\Borg.SubSpace\Borg.SubSpace\Borg.SubSpace.csproj" };
#endif

            string pluginoutputpath = "";
            string projectpath = "";
            string baseAssemblyName = ""; //Derived from AssemblyTitle in your plugin's AssemblyInfo.cs
            string guid = ""; //Derived from the Guid in your plugin's AssemblyInfo.cs

            //Load metadata
            try
            {
                if (args.Count() != 3)
                    throw new InvalidOperationException("Invalid arguments.");

                //Load plugin output path
                pluginoutputpath = args[1];

                //Load project path (full path to .csproj file)
                projectpath = args[2];

                //Load project xml
                CSProjXmlDoc = XDocument.Load(projectpath);

                //Load assembly guid and name from AssemblyInfo.cs
                var assemblyInfoPath = Path.GetDirectoryName(projectpath) + "/Properties/AssemblyInfo.cs";
                baseAssemblyName = GetAssemblyTitle(assemblyInfoPath);
                guid = ReadGuid(assemblyInfoPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
                Console.WriteLine("ERROR: Unable to parse plugin.");
                Environment.Exit(1);
            }
            Process p;
            
            if (args[0] == "pre")
            {
                //Unregister plugin
                UnregisterPlugin(guid);

                //Delete files from disc, make sure folder ends with debug
                try
                {
                    var folder = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(pluginoutputpath), ".."));
                    if (folder.EndsWith("bin\\Debug"))
                        Directory.Delete(Path.GetDirectoryName(folder), true);
                }
                catch { }
            }

            else if (args[0] == "post")
            {
                System.Threading.Thread.Sleep(100);

                #if DEBUG
                if (!File.Exists(pluginoutputpath))
                {
                    //Compilation must have failed so unregister plugin
                    UnregisterPlugin(guid);
                    return;
                }

                //Load PluginCore type - This is only necessary to get the "friendly plugin name"
                Assembly a = Assembly.LoadFile(pluginoutputpath);
                Type t = null;

                var fulltypename = baseAssemblyName + ".PluginCore";
                t = a.GetType(fulltypename);
                if (t == null || !t.IsSubclassOf(typeof(PluginBase)))
                    throw new InvalidOperationException("Unable to find single class named " + fulltypename + " that inherits from PluginBase");

                //Load plugin name
                var friendlyatt = t.GetCustomAttributes(typeof(FriendlyNameAttribute), false).SingleOrDefault() as FriendlyNameAttribute;

                //Load paths for decal registration
                string pluginpath = Path.GetDirectoryName(pluginoutputpath).Replace(@"\", @"\\");
                string pluginassembly = Path.GetFileName(pluginoutputpath);

                //Register plugin
                
                RegisterPlugin(friendlyatt.Name, guid, t.Namespace, t.Name, pluginpath, pluginassembly);

                //Overwrite csproj filename to give assembly a unique name for next build
                RandomizeAssemblySuffix(baseAssemblyName, projectpath);
                #else
                //Revert csproj changes back to normal
                UpdateAssemblyNameForBuild("Debug", baseAssemblyName);
                UpdateDebugOutputPath();
                CSProjXmlDoc.Save(projectpath);
                #endif
            }
        }

        /// <summary>
        /// Loads the plugin assembly target location from the csproj file
        /// </summary>
        /// <returns></returns>
        private static string GetPluginAssemblyPath()
        {
            //Get all property group elements
            var pglist_all = CSProjXmlDoc.Elements()
                .First()
                .XGetByName("PropertyGroup");
            throw new NotImplementedException();
        }

        /// <summary>
        /// Parses the assembly title from the AssemblyInfo.cs file, given the path
        /// </summary>
        /// <param name="assemblyInfoPath"></param>
        /// <returns></returns>
        private static string GetAssemblyTitle(string assemblyInfoPath)
        {
            foreach (var line in File.ReadAllLines(assemblyInfoPath))
            {
                if (line.StartsWith("[assembly: AssemblyTitle(\""))
                {
                    int start = line.IndexOf('"')+1;
                    int end = line.LastIndexOf('"');
                    return line.Substring(start, end - start);
                }
            }
            throw new InvalidOperationException("Unable to parse AssemblyName from " + assemblyInfoPath);
        }

        /// <summary>
        /// Parses the plugin GUID from the AssemblyInfo.cs file, given the path
        /// </summary>
        /// <param name="assemblyInfoPath"></param>
        /// <returns></returns>
        private static string ReadGuid(string assemblyInfoPath)
        {
            foreach (var line in File.ReadAllLines(assemblyInfoPath))
            {
                if (line.StartsWith("[assembly: Guid(\""))
                {
                    int start = line.IndexOf('"')+1;
                    int end = line.LastIndexOf('"');
                    return line.Substring(start, end - start);
                }
            }
            throw new InvalidOperationException("Unable to parse Guid from " + assemblyInfoPath);
        }

        /// <summary>
        /// Gives a unique name to the assembly, so that it can be loaded without shutting down AC
        /// Returns the random integer suffix used in 
        /// </summary>
        /// <param name="projectpath"></param>
        private static int RandomizeAssemblySuffix(string baseAssemblyName, string projectPath)
        {
            //Generate assembly name
            int random_suffix = new Random().Next();
            string assemblyname = string.Format(@"{0}_{1}", baseAssemblyName, random_suffix);

            //Remove the unconditional assembly name from the C# Project config
            RemoveUnconditionalAssemblyName();

            //Update assembly names in CSProj file
            UpdateAssemblyNameForBuild("Debug", assemblyname);
            UpdateAssemblyNameForBuild("Release", baseAssemblyName);

            UpdateDebugOutputPath(random_suffix);

            //Save
            CSProjXmlDoc.Save(projectPath);

            return random_suffix;
        }

        /// <summary>
        /// Removes the unconditional assembly name from the C# Project config
        /// </summary>
        private static void RemoveUnconditionalAssemblyName()
        {
            //Get all property group elements
            var pglist_all = CSProjXmlDoc.Elements()
                .First()
                .XGetByName("PropertyGroup");

            //Determine if we need to remove the assembly name from the base project config (and make it conditional on debug or release mode)
            var pgitems_nonconditional = pglist_all
                .Where(x => x.Attributes().Count() == 0)
                .SelectMany(x => x.Elements())
                .ToList();

            //Find all AssemblyName attributes from the propertygroups that do not have a build condition
            var pgmain = pgitems_nonconditional.Where(x => x.Name.LocalName == "AssemblyName").ToList();

            //And remove them
            pgmain.ForEach(x => x.Remove());
        }

        /// <summary>
        /// Updates the assembly name xml element, corresponding to the project build mode
        /// </summary>
        /// <param name="buildType"></param>
        /// <param name="assemblyname"></param>
        private static void UpdateAssemblyNameForBuild(string buildType, string assemblyname)
        {
            var pglist = CSProjXmlDoc.Elements()
                .First() //Get root element
                .XGetByName("PropertyGroup") //Get all propertygroup children
                .Where(x =>
                    x.Attributes() //Target the attribute of the PropertyGroup elements
                    .Select(y => y.Value) //Select the attribute text
                    .Any(z => z.Contains("'" + buildType + "|"))) //Reduce whole attribute to match the build type
                .ToList();

            //Find the element we are changing, if it exists
            var element_assemblyname = pglist
                .SelectMany(x => x.Elements()) //Flatten the list as we may have our debug propertygroup defined in multiple sectors
                .Where(x => x.Name.LocalName == "AssemblyName")
                .SingleOrDefault();

            //Assembly name not found in debug config
            if (element_assemblyname == null)
            {
                //Create and add the AssemblyName tag to our csproj debug build
                var _namespace = CSProjXmlDoc.Elements().First().Name.Namespace.NamespaceName;
                var xname = XName.Get("AssemblyName", _namespace);
                var newElement = new XElement(xname, assemblyname);

                pglist.First().Add(newElement);
            }
            else
            {
                //Update the existing assembly name to our random value
                element_assemblyname.Value = assemblyname;
            }
        }

        /// <summary>
        /// Updates the debug project output path, to give the build path a unique name
        /// </summary>
        /// <param name="random_suffix"></param>
        private static void UpdateDebugOutputPath(int random_suffix = 0)
        {
            var pglist = CSProjXmlDoc.Elements()
               .First() //Get root element
               .XGetByName("PropertyGroup") //Get all propertygroup children
               .Where(x =>
                   x.Attributes() //Target the attribute of the PropertyGroup elements
                   .Select(y => y.Value) //Select the attribute text
                   .Any(z => z.Contains("'Debug|"))) //Reduce whole attribute to match the build type
               .ToList();

            //Find the output path
            var element_outputpath = pglist
                .SelectMany(x => x.Elements()) //Flatten the list as we may have our debug propertygroup defined in multiple sectors
                .Where(x => x.Name.LocalName == "OutputPath")
                .Single();

            //Change output path
            string newOutputpath = "bin\\Debug" + (random_suffix > 0 ? "\\" + random_suffix.ToString() : "");
            element_outputpath.Value = newOutputpath;
        }

        private static List<XElement> XGetByName(this XElement element, string name)
        {
            return element.Elements().Where(x => x.Name.LocalName == name).ToList();
        }

        /// <summary>
        /// Registers the plugin with decal, via running a .reg script
        /// </summary>
        /// <param name="pluginname"></param>
        /// <param name="guid"></param>
        /// <param name="pluginnamespace"></param>
        /// <param name="plugintype"></param>
        /// <param name="pluginpath"></param>
        /// <param name="pluginassembly"></param>
        static void RegisterPlugin(string pluginname, string guid, string pluginnamespace, string plugintype, string pluginpath, string pluginassembly)
        {
            var text = GetEmbeddedTextFile("Borg.DecalLoader.Register.txt");
            text = text.Replace("{GUID}", guid);
            text = text.Replace("{PLUGINNAME}", pluginname);
            text = text.Replace("{PLUGINNAMESPACE}", pluginnamespace);
            text = text.Replace("{PLUGINTYPE}", plugintype);
            text = text.Replace("{PLUGINPATH}", pluginpath);
            text = text.Replace("{PLUGINASSEMBLY}", pluginassembly);

            RunRegFile(text);
        }

        /// <summary>
        /// Unregisters the plugin with the given guid from Decal
        /// </summary>
        /// <param name="guid"></param>
        static void UnregisterPlugin(string guid)
        {
            var text = GetEmbeddedTextFile("Borg.DecalLoader.Unregister.txt");
            text = text.Replace("{GUID}", guid);

            RunRegFile(text);
        }

        /// <summary>
        /// Turns a text file into a registry file, then runs it
        /// </summary>
        /// <param name="text"></param>
        private static void RunRegFile(string text)
        {
            string filename = "tempreg.reg";
            string fullname = Path.GetFullPath(filename);
            Console.WriteLine("Wrote regfile to " + fullname);
            File.WriteAllText(fullname, text);
            Process regeditProcess = Process.Start("regedit.exe", "/s " + fullname);
            regeditProcess.WaitForExit();
            File.Delete(filename); 
        }

        /// <summary>
        /// Retrieves an embedded resource from this assembly
        /// </summary>
        /// <param name="resourceName"></param>
        /// <returns></returns>
        static string GetEmbeddedTextFile(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                string result = reader.ReadToEnd();
                return result;
            }
        }
    }
}
